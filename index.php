<? require 'include/index_controller.php' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="styles.css" rel="stylesheet" />
    <title>Project - ведение списков</title>
</head>

<body>

<div class="header">
    <div class="logo"><img src="i/logo.png" width="68" height="23" alt="Project" /></div>
    <div style="clear: both"></div>
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="left-collum-index">

            <h1>Возможности проекта —</h1>
            <p>Вести свои личные списки, например покупки в магазине, цели, задачи, и многое другое.
                Делиться списками с друзьями, и просматривать списки друзей.</p>


        </td>
        <td class="right-collum-index">
            <? if ($isShowAuthForm && !$isAuthorized): ?>
                <div class="project-folders-menu">
                    <ul class="project-folders-v">
                        <li class="project-folders-v-active"><span>Авторизация</span></li>
                        <li><a href="#">Регистрация</a></li>
                        <li><a href="#">Забыли пароль?</a></li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>
                <? if ($isFail): ?>
                    <? include 'include/fail.php' ?>
                <? endif ?>
                <form method="post" action="index.php?login=yes" name="auth">
                    <div class="index-auth">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="iat">Ваш e-mail:
                                    <br />
                                    <input id="login_id" size="30" name="login" value="<?= $login ?? '' ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="iat">Ваш пароль:
                                    <br />
                                    <input id="password_id" size="30" name="password" value="<?= $password ?? '' ?>"
                                      type="password"/>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="submit" value="Войти" /></td>
                            </tr>
                        </table>
                    </div>
                </form>
            <? endif ?>
            <? if ($isAuthorized): ?>
                <? include 'include/success.php' ?>
            <? endif ?>
        </td>
    </tr>
</table>

<div class="footer">
    &copy;&nbsp;<nobr>2018</nobr> Project.
</div>

</body>
</html>

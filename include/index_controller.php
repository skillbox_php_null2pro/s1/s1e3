<?
include 'logins.php';
include 'passwords.php';

//дефолтный логин
$defLogin = 'admin';
$defPassword = 'admin';

$isAuthorized = false;
$isFail = false;

//Поля авторизации заполнены?
$isAuthorizable = !empty($_POST['login']) && !empty($_POST['password']);

//Показываем форму авторизации?
$isShowAuthForm = !empty($_GET['login']) && $_GET['login'] === 'yes';

//Авторизация
if ($isAuthorizable) {
    //Зачищаем ввод
    $login = trim(htmlspecialchars($_POST['login']));
    $password = trim(htmlspecialchars($_POST['password']));

    if ($login === $defLogin && $password === $defPassword) {
        $isAuthorized = true;
    } elseif (in_array($login, $arLogins, true)) {
        $idx = array_search($login, $arLogins, true);

        if ($idx !== false) {
            if ($password === $arPasswords[$idx]) {
                $isAuthorized = true;
            }
        }
    }

    if (!$isAuthorized) {
        $isFail = true;
    }
}
